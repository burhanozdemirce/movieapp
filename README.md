# Giriş

The Movie DB film listeleme IOS uygulaması

# Web servis kaynağı

- https://developers.themoviedb.org/3 

# Başlarken

Uygulama MVC pattern kullanıldı.
Web servisten data çekmek için standart URLRequest kullandım. Sadece iki servis ağrısı yaptığım için ApiClient Class'ını basit tuttum. Api sayısı fazla olsaydı kod tekrarını engelleyecek şekilde düzeltmeler yapabiirdim.
Web servisten dönen jsonları modelime çevirdim.

Image lar için URLSession -> dataTask kullandım. Yüklenen imageları cache url bilgisi ile yazıp, network kullanımını ve yyüklenme sürelerini azalttım. 
UIImageView için Extension yazarak daha kolay ve temiz kod olmasını sağladım.
Favori listesini Userdefault'ta tuttum. 
Uygulama içinde rahat erişim için, SharedData adında class oluşturup (singleton), favori listesini uygulama ilk açıldığında buraya setledim. Uygulama içerisinde bu liste üzerinden işlem yaptım. 
MovieServices classında, favoriye ekleme, çıkarma ve favori kontrolü işlemlerini burada yaptım.

CollectionView de favoriye ekleme ve çıkarma işlemini cell'in kendi classında yaptım 
Detay sayfasında favoriye ekleme veya çıkarma işlemlerinde NotificationCenter ile collectionview'in ilgili itemının reload edilmesini sağladım. 

Listeleme ekranında UISearchController kullanıp yüklenen filmleri filtrelettim.
