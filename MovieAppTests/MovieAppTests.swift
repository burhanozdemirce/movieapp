//
//  MovieAppTests.swift
//  MovieAppTests
//
//  Created by Burhan Özdemir on 24.01.2021.
//

import XCTest
@testable import MovieApp

class MovieAppTests: XCTestCase {

    let testMovieId = 112233
    let testMovieIdNotFavorite = 11223300
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        SharedData.shared.favoriteList = UserDefaults.standard.object(forKey: MovieServices.favoriteListKey) as? [Int] ?? [Int]()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        MovieServices.removeMovieFromFavourite(movieId: testMovieId)
    }
    
    func testAddFavorite() {
        MovieServices.addMovieToFavourite(movieId: testMovieId)
        XCTAssertTrue(MovieServices.isMovieInFavourite(movieId: testMovieId))
    }
    
    
    func testRemoveFavorite() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        MovieServices.addMovieToFavourite(movieId: testMovieId)
        MovieServices.removeMovieFromFavourite(movieId: testMovieId)
        XCTAssertFalse(MovieServices.isMovieInFavourite(movieId: testMovieId))
    }
    
    func testIsInFavorite() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        MovieServices.addMovieToFavourite(movieId: testMovieId)
        XCTAssertTrue(MovieServices.isMovieInFavourite(movieId: testMovieId))
        XCTAssertFalse(MovieServices.isMovieInFavourite(movieId: testMovieIdNotFavorite))
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}
