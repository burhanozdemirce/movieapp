//
//  UIImageView+Extension.swift
//  MovieApp
//
//  Created by Burhan Özdemir on 23.01.2021.
//

import UIKit

enum ImageSize : Int {
    case smallSize = 200
    case largeSize = 500
}

let imageBasePath = "https://image.tmdb.org/t/p/w%d%@"

extension UIImageView {
    func loadImageWith(posterPath: String, imageSize: ImageSize) {
        self.image = nil
        if posterPath.isEmpty {
            self.image = UIImage.init(named: "broken-image")
            return
        }
        
        let urlString = String.init(format: imageBasePath, imageSize.rawValue, posterPath)
        if let imageUrl = URL(string: urlString) {
            //download image from url
            URLSession.shared.dataTask(with: imageUrl, completionHandler: { (data, response, error) in
                DispatchQueue.main.async {
                    if error != nil {
                        self.image = UIImage.init(named: "broken-image")
                        print(error!)
                        return
                    }
                    
                    if let image = UIImage(data: data!) {
                        self.image = image
                    }
                }
            }).resume()
            
        }else{
            self.image = UIImage.init(named: "broken-image")
        }
    }
}
