//
//  MovieServices.swift
//  MovieApp
//
//  Created by Burhan Özdemir on 24.01.2021.
//

import Foundation

class MovieServices {
    static let favoriteListKey = "favoriteListKey"
    
    static func addMovieToFavourite(movieId: Int) {
        let defaults = UserDefaults.standard
        if !SharedData.shared.favoriteList.contains(movieId){
            SharedData.shared.favoriteList.append(movieId)
        }
        defaults.set(SharedData.shared.favoriteList, forKey: favoriteListKey)
    }
    
    static func removeMovieFromFavourite(movieId: Int) {
        let defaults = UserDefaults.standard
        if SharedData.shared.favoriteList.contains(movieId){
            SharedData.shared.favoriteList.removeAll(where: { $0 == movieId })
        }
        defaults.set(SharedData.shared.favoriteList, forKey: favoriteListKey)
    }
    
    static func isMovieInFavourite(movieId: Int) -> Bool {
        return SharedData.shared.favoriteList.contains(movieId)
    }
    
}
