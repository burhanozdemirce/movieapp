//
//  MovieApiClient.swift
//  MovieApp
//
//  Created by Burhan Özdemir on 23.01.2021.
//

import Foundation

class MovieApiClient {
    
    private let apiUrl = "https://api.themoviedb.org/3/movie/"
    private let apiKey = "fd2b04342048fa2d5f728561866ad52a"
    private let language = "en-US"
    
    func getMovies(page: Int, completion: @escaping (Result<MoviesResponse, Error>) -> Void) {
        let requestUrl = String.init(format: "%@popular?language=%@&api_key=%@&page=%d", apiUrl, language, apiKey, page)
        
        guard let moviesUrl = URL(string: requestUrl) else { fatalError() }
        let moviesUrlRequest = URLRequest(url: moviesUrl)
        
        let dataTask = URLSession.shared.dataTask(with: moviesUrlRequest) { (data, _, error) in
            if let error = error {
                completion(.failure(error))
            } else if let data = data {
                let decoder = JSONDecoder()
                do {
                    let moviesResponse = try decoder.decode(MoviesResponse.self, from: data)
                    completion(.success(moviesResponse))
                } catch {
                    completion(.failure(error))
                }
            }
        }
        dataTask.resume()
    }
    
    func getMovieDetail(movieId: Int, completion: @escaping (Result<Movie, Error>) -> Void) {
        let requestUrl = String.init(format: "%@%d?language=%@&api_key=%@", apiUrl, movieId, language, apiKey)
        guard let moviesUrl = URL(string: requestUrl) else { fatalError() }
        let moviesUrlRequest = URLRequest(url: moviesUrl)
        
        let dataTask = URLSession.shared.dataTask(with: moviesUrlRequest) { (data, _, error) in
            if let error = error {
                completion(.failure(error))
            } else if let data = data {
                let decoder = JSONDecoder()
                do {
                    let movieDetail = try decoder.decode(Movie.self, from: data)
                    completion(.success(movieDetail))
                } catch {
                    completion(.failure(error))
                }
            }
        }
        dataTask.resume()
    }
}
