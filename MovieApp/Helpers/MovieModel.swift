//
//  MovieModel.swift
//  MovieApp
//
//  Created by Burhan Özdemir on 23.01.2021.
//

import Foundation

// MARK: - Movie
struct Movie: Codable {
    let backdropPath: String?
    let id: Int
    let overview: String
    let posterPath: String?
    let title: String
    let voteAverage: Double
    let voteCount: Int

    enum CodingKeys: String, CodingKey {
        case backdropPath = "backdrop_path"
        case id
        case overview
        case posterPath = "poster_path"
        case title
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
    }
}
