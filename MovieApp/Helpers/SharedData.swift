//
//  SharedData.swift
//  MovieApp
//
//  Created by Burhan Özdemir on 24.01.2021.
//

import Foundation

class SharedData{
    
    static let shared = SharedData()
    var favoriteList = [Int]()
    
    private init(){}
}
