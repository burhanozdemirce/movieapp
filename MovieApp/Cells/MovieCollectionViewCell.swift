//
//  MovieCollectionViewCell.swift
//  MovieApp
//
//  Created by Burhan Özdemir on 23.01.2021.
//

import UIKit

//protocol MovieCollectionViewCellDelegate: class {
//    func starButtonTapped(isStarred: Bool);
//}

class MovieCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var movieNameLabel: UILabel!
    @IBOutlet weak var moviePosterImageView: UIImageView!
    @IBOutlet weak var starButton: UIButton!
    
    var movieDetail: Movie?
    
    //weak var delegate: MovieCollectionViewCellDelegate?
    
    static let cellIdentifier = "MovieCollectionViewCell"
    
    class func instanceFromNib() -> UINib {
        return UINib(nibName: MovieCollectionViewCell.cellIdentifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
    }
    
    func initWithMovie(movie: Movie) -> Void {
        self.movieDetail = movie
        self.movieNameLabel.text = movie.title
        self.moviePosterImageView.loadImageWith(posterPath: movie.posterPath ?? "", imageSize: .smallSize)
        self.starButton.isSelected = MovieServices.isMovieInFavourite(movieId: movie.id)
    }

    @IBAction func starButtonTapped(_ sender: Any) {
        self.starButton.isSelected = !self.starButton.isSelected
        if self.starButton.isSelected {
            MovieServices.addMovieToFavourite(movieId: movieDetail!.id)
        }else{
            MovieServices.removeMovieFromFavourite(movieId: movieDetail!.id)
        }
    }
}
