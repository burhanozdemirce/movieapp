//
//  MovieDetailViewController.swift
//  MovieApp
//
//  Created by Burhan Özdemir on 23.01.2021.
//

import UIKit

class MovieDetailViewController: UIViewController {
    @IBOutlet weak var moviePosterImageView: UIImageView!
    @IBOutlet weak var movieNameLabel: UILabel!
    @IBOutlet weak var movieOverviewTextView: UITextView!
    @IBOutlet weak var starButtonItem: UIBarButtonItem!
    @IBOutlet weak var voteCountLabel: UILabel!
    @IBOutlet weak var voteAverageLabel: UILabel!
    
    var movieDetail : Movie?
    var isStarred: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUI()
        
        //TODO: Detay Servisi request iptal
        //Detay ekranında gösterilen bilgiler listeden geldiği için
        //Aynı bilgiler için tekrar request göndermedim.
        //getMovieDetail()
    }
    
    func setUI() {
        moviePosterImageView.loadImageWith(posterPath: movieDetail?.backdropPath ?? "", imageSize: .largeSize)
        movieNameLabel.text = movieDetail?.title
        movieOverviewTextView.text = movieDetail?.overview
        voteCountLabel.text = String.init(movieDetail!.voteCount)
        voteAverageLabel.text = String.init(format: "%.1f", movieDetail!.voteAverage)
        
        isStarred = MovieServices.isMovieInFavourite(movieId: movieDetail!.id)
        setStarButtonIcon()
    }
    
    func setStarButtonIcon() {
        if isStarred {
            starButtonItem.image = UIImage.init(systemName: "star.fill")
        }else{
            starButtonItem.image = UIImage.init(systemName: "star")
        }
        NotificationCenter.default.post(name: MoviesCollectionViewController.onStaredChangeNotificationName, object: nil, userInfo:["id": movieDetail!.id])
    }
    
    @IBAction func starButtonTapped(_ sender: Any) {
        isStarred = !isStarred
        if isStarred {
            MovieServices.addMovieToFavourite(movieId: movieDetail!.id)
        }else{
            MovieServices.removeMovieFromFavourite(movieId: movieDetail!.id)
        }
        setStarButtonIcon()
    }
    
    func getMovieDetail(){
        MovieApiClient.init().getMovieDetail(movieId: movieDetail!.id) { (result) in
            switch result {
            case .success(let movieDetailModel):
                self.movieDetail = movieDetailModel
                DispatchQueue.main.async {
                    self.setUI()
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
