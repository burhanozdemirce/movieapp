//
//  MoviesCollectionViewController.swift
//  MovieApp
//
//  Created by Burhan Özdemir on 23.01.2021.
//

import UIKit

class MoviesCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UISearchResultsUpdating {

    var totalResults = 0
    var pageIndex = 1
    var movies: [Movie] = []
    var filteredMovies: [Movie] = []
    var selectedMovie: Movie?
    let movieDetailSegueId = "toMovieDetail"
    var isFiltered = false
    
    static let onStaredChangeNotificationName = Notification.Name("onStaredChangeNotificationName")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Register cell classes
        self.collectionView!.register(MovieCollectionViewCell.instanceFromNib(), forCellWithReuseIdentifier: MovieCollectionViewCell.cellIdentifier)

        // Do any additional setup after loading the view.
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 10
        layout.sectionInset.right = 10
        layout.sectionInset.left = 10
        layout.sectionInset.top = 10
        layout.sectionInset.bottom = 10
        collectionView.setCollectionViewLayout(layout, animated: true)
        NotificationCenter.default.addObserver(self, selector: #selector(onStaredChangeNotification(notification:)), name: MoviesCollectionViewController.onStaredChangeNotificationName, object: nil)
        
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        self.navigationItem.searchController = searchController
        searchController.obscuresBackgroundDuringPresentation = false
        
        getMovies()
    }
    
    func getMovies(){
        MovieApiClient.init().getMovies(page: pageIndex) { result in
            switch result {
            case .success(let moviesResult):
                self.movies.append(contentsOf: moviesResult.movies)
                self.filteredMovies = self.movies
                self.totalResults = moviesResult.totalResults
                self.pageIndex = self.pageIndex + 1
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
                print("\(moviesResult.totalResults)")
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    @objc func onStaredChangeNotification(notification:Notification)
    {
        let movieId = notification.userInfo!["id"] as? Int
        if let itemIndex = filteredMovies.firstIndex(where: { $0.id == movieId}){
            collectionView.reloadItems(at: [IndexPath.init(item: itemIndex, section: 0)])
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == movieDetailSegueId {
            let movieDetailVC = segue.destination as! MovieDetailViewController
            movieDetailVC.movieDetail = selectedMovie
        }
    }
    
    // MARK: UISearchResultsUpdating
    
    func updateSearchResults(for searchController: UISearchController) {
        let searchText = searchController.searchBar.text ?? ""
        
        if searchText.isEmpty {
            filteredMovies = movies
            isFiltered = false
        }else{
            //Search on only fetched
            filteredMovies = movies.filter{ $0.title.lowercased().contains(searchText.lowercased()) }
            isFiltered = true
        }
        collectionView.reloadData()
    }
    

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return filteredMovies.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieCollectionViewCell.cellIdentifier, for: indexPath) as! MovieCollectionViewCell
        // Configure the cell
        let movie = filteredMovies[indexPath.item]
        cell.initWithMovie(movie: movie)
        return cell
    }

    // MARK: UICollectionViewDelegate

    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        //dont fetch when filtered
        if !isFiltered && indexPath.row == collectionView.numberOfItems(inSection: indexPath.section) - 1{
            if movies.count < totalResults {
                getMovies()
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedMovie = filteredMovies[indexPath.item]
        self.performSegue(withIdentifier: movieDetailSegueId, sender: self)
    }
    
    // MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
        let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
        let size:CGFloat = (collectionView.frame.size.width - space) / 2
        return CGSize(width: size, height: size * 1.5)
    }

}
